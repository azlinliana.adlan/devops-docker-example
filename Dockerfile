FROM python:3.9
WORKDIR /app
COPY . /app
RUN pip install DateTime
EXPOSE 8000
CMD ["python", "py_test.py"]